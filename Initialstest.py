from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)


# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values()
print(joint_goal)
joint_goal[0] = -0.15
joint_goal[1] = -0.96
joint_goal[2] = 1.1
joint_goal[3] = 0.0
joint_goal[4] = 0.0
joint_goal[5] = 0.0
# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()

waypoints = []
scale = 1.0
wpose = move_group.get_current_pose().pose 
wpose.position.z += scale * 0.1  # move up (z)
waypoints.append(copy.deepcopy(wpose)) # up line for D

wpose.position.y += scale * 0.1   # right (y)
wpose.position.z -= scale * 0.05   # and down (z)
waypoints.append(copy.deepcopy(wpose)) # oversimplified downward right slash for part of curve

wpose.position.y -= scale * 0.1  # move left (y)
wpose.position.z -= scale * 0.05  # move down (z)
waypoints.append(copy.deepcopy(wpose)) #downward left slash for part of curve D
# Finish letter D Initial
#Start Letter A
wpose.position.y += scale * 0.1  # move right (y) to start new letter
waypoints.append(copy.deepcopy(wpose))

wpose.position.y += scale * 0.05   # right (y)
wpose.position.z += scale * 0.1   # and up (z)
waypoints.append(copy.deepcopy(wpose)) #upward right slash for A

wpose.position.y += scale * 0.05  # move right (y)
wpose.position.z -= scale * 0.1  # move down (z)
waypoints.append(copy.deepcopy(wpose)) # downward right slash for A

wpose.position.z += scale * 0.05 #move up for crossing A (z)
waypoints.append(copy.deepcopy(wpose))

wpose.position.y -= scale * 0.1 #cross for A (y)
waypoints.append(copy.deepcopy(wpose))
#Finish Letter A Initial
#Start Letter E
wpose.position.z -= scale * 0.05 #move down (z)
waypoints.append(copy.deepcopy(wpose)) 

wpose.position.y += scale * 0.15 #move right to start point of new letter (y)
waypoints.append(copy.deepcopy(wpose))

wpose.position.y += scale * 0.1 #move right to draw bottom of E
waypoints.append(copy.deepcopy(wpose))

wpose.position.y -= scale * 0.1 #move left to start of E
waypoints.append(copy.deepcopy(wpose))

wpose.position.z += scale * 0.05 #move up for left vertical length of E
waypoints.append(copy.deepcopy(wpose)) 

wpose.position.y += scale * 0.05 #move left to draw middle line
waypoints.append(copy.deepcopy(wpose))

wpose.position.y -= scale *0.05 #move back to left vertical length of E
waypoints.append(copy.deepcopy(wpose))
 
wpose.position.z += scale*0.05 #move up to top position of E
waypoints.append(copy.deepcopy(wpose))

wpose.position.y += scale*0.1 #move right to finish top of E
waypoints.append(copy.deepcopy(wpose))
#Finished Letter E Initial

# We want the Cartesian path to be interpolated at a resolution of 1 cm
# which is why we will specify 0.01 as the eef_step in Cartesian
# translation.  We will disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space, which is sufficient
# for this tutorial.
(plan, fraction) = move_group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

# Note: We are just planning, not asking move_group to actually move the robot yet:


display_trajectory = moveit_msgs.msg.DisplayTrajectory()
display_trajectory.trajectory_start = robot.get_current_state()
display_trajectory.trajectory.append(plan)
# Publish
display_trajectory_publisher.publish(display_trajectory)


move_group.execute(plan, wait=True)
