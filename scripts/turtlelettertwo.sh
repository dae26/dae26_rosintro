
#!/usr/bin/bash
rosservice call /reset
rosservice call turtle1/set_pen 1 0 0 10 1
rosservice call /turtle1/teleport_absolute 1 5 0
rosservice call turtle1/set_pen 100 0 0 10 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[6.5, 0.0 ,0.0]' '[0.0, 0.0, -3.0]'
rosservice call turtle1/set_pen 1 0 0 10 1
rosservice call /turtle1/teleport_absolute 5 5 0
rosservice call turtle1/set_pen 100 100 0 10 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, .0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-4.0, 0.0 ,0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4, 0.0 ,0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-4.0, 0.0 ,0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 2.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4, 0.0 ,0.0]' '[0.0, 0.0, 0.0]'
